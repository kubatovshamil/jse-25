package ru.t1.kubatov.tm.exception.user;

import ru.t1.kubatov.tm.exception.system.AbstractSystemException;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
