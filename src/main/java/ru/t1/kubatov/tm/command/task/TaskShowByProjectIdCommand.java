package ru.t1.kubatov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Show Task by Project ID.";

    @NotNull
    public final static String NAME = "task-show-by-project-id\"";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("Enter Project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectID(getUserId(), projectId);
        showTaskList(tasks);
        System.out.println("[OK]");
    }
}
