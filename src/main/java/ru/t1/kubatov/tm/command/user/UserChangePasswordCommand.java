package ru.t1.kubatov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "Edit User.";

    @NotNull
    public final static String NAME = "user-edit";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("Enter new Password:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
