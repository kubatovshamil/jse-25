package ru.t1.kubatov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Update Task by index.";

    @NotNull
    public final static String NAME = "task-update-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        getTaskService().updateByIndex(getUserId(), index, name, description);
    }
}
